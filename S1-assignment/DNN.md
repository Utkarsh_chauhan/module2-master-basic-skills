# Deep Neural Network (DNN):

A deep neural network (DNN) is an artificial neural network (ANN) with multiple layers between the input and output layers. The DNN finds the correct mathematical manipulation to turn the input into the output, whether it be a linear relationship or a non-linear relationship.

There are 3 layers in DNN, which helps in learning of the model.
1. Input layer
1. Hidden layer
1. output layer

![](https://img.securityinfowatch.com/files/base/cygnus/siw/image/2019/02/Figure_01.5c7712513151e.png?auto=format&w=720)

As shown in the above picture, the only difference between the simple NN and DNN is that of the number of hidden layers.
There are multiple layers in the hidden layer of DNN while there is just a single layer of hidden layer.

## Working of DNN:

The working of DNN is similar to the working of a human brain. In human brain, we have neurons while in DNN we have nodes.
Our brain learns things by assigning and adjusting the weights to a particular feature. Similarly, DNN too assigns weights
to a feature and adjusts it multiple times during its training phase and learns through these adjustments.

![](https://www.quantib.com/hs-fs/hubfs/assets/pillar%20pages/The%20ultimate%20guide%20to%20AI%20in%20radiology/images/Pillar%20page%20-%20figure%2011-2.png?width=1149&name=Pillar%20page%20-%20figure%2011-2.png)

As depicted in the diagram above, when an input is given, that input goes to the hidden layer of DNN.
In this hidden layer of DNN, weights are assigned to every single feature. 
After the assignment of weights, DNN predicts the output refering to its assigned weights. More the weight of a feature
more is its priority. That means, for prediction, the feature with the highest priority will be considered first followed bu others.

![](https://images.deepai.org/glossary-terms/73eec54be08746f6b546a874580b8673/backpropagation.png)

The assigned weights are not the final weights. Adjustments are made in the weights in order to increase the accuracy of the prediction.
This can be done through Back propagation technique, like shown above.
In this weights are rechecked and adjusted at every layer of the hidden layer. 

The overall working of DNN can be shown through the below diagram - 

![](https://i.stack.imgur.com/7Ui1C.png)

After hidden layer, an activation function is there, in order to get the output in required form.