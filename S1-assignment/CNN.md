# Convolution Neural Networks (CNN):

It is a Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other.

There are mainly 4 steps in CNN:
1. Convolution
1. Max Pooling
1. Flattenning
1. Full Connection

## Convolution Operation:

*(INPUT IMAGE) x (FILTER/FEATURE DETECTION) = FEATURE MAP*

Here multiple filters are used - combined term = **Convolution layer**
This process bascially reduces the size of the image.

![CNN](https://qph.fs.quoracdn.net/main-qimg-5bfbe970b2bf646781b66e9e78a2214b)

Consider the above picture, there the input image is being filtered using filters.
The filter moves to the right with a certain Stride Value till it parses the complete width. Moving on, it hops down to the beginning (left) of the image with the same Stride Value and repeats the process until the entire image is traversed.

![](https://miro.medium.com/max/1300/1*CfNnqc6aEchW56uSlWdpxw.gif)


In the case of images with multiple channels (e.g. RGB), the Kernel has the same depth as that of the input image. Matrix Multiplication is performed between Kn and In stack ([K1, I1]; [K2, I2]; [K3, I3]) and all the results are summed with the bias to give us a squashed one-depth channel Convoluted Feature Output.

From both the case we get output as a feature map.

**ReLU layer is also applied on Convolution layer in irder to increease the non-linearity.**

## Max Pooling:
It is used to reduce the size as well as to recogonise every picture of different size, shape, texture.

FEATURE MAP ----Max Pooling----> POOLED FEATURE MAP


![](https://i2.wp.com/principlesofdeeplearning.com/wp-content/uploads/2018/07/poolingmax.png?resize=600%2C264)


In this we take the maximum value of each pixel, and take that max value as the input pixel for the pooled feature map.
Like in the above picture, Considering a block of 4 pixels, the max value is extracted and taken as input for the pooled featured map.


## Flattenning:

POOLED FEATURED MAP ---- flattenning---> Flattened 

![](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/73_blog_image_2.png)

In the above image pooled featured map is being flattened
This flattened output is used as an input for the DNN/ANN.

## Full Connection:

![](https://www.mdpi.com/sensors/sensors-19-04933/article_deploy/html/images/sensors-19-04933-g001.png)

After flattening of the image, this flattened image becomes the input for the ANN, so basically we are connecting 
CNN with ANN.