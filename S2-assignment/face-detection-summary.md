# Face - Detection:

Face detection applications use algorithms and ML to find human faces within larger images, which often incorporate other non-face objects such as landscapes, buildings and other human body parts like feet or hands. Face detection algorithms typically start by searching for human eyes -- one of the easiest features to detect. The algorithm might then attempt to detect eyebrows, the mouth, nose, nostrils and the iris. Once the algorithm concludes that it has found a facial region, it applies additional tests to confirm that it has, in fact, detected a face, like enclosing the face in 
box.


![](https://cdn.technologyreview.com/i/images/Face%20detection.png)


**Some of the more specific techniques used in face detection include:**

1. Removing the background. For example, if an image has a plain, mono-color background or a pre-defined, static background, then removing the background can help reveal the face boundaries.
1. In color images, sometimes skin color can be used to find faces; however, this may not work with all complexions.
1. Using motion to find faces is another option. In real-time video, a face is almost always moving, so users of this method must calculate the moving area. One drawback of this method is the risk of confusion with other objects moving in the background.
1. A combination of the strategies listed above can provide a comprehensive face detection method.

## How to identify the face features:

![](https://bloggrijjy.files.wordpress.com/2017/09/facedetection.png)

As depicted in the above picture, each pixel is to be considered while deciding the features of a person.
For eg:
if a model have decided the location of the eye in a particular picture, then it will mark as 1 to that pixel. and other surrounding will be
unmarked. So, a system gets an input in the form of 1's and 0's.


