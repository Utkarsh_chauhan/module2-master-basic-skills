# Face - Landmarks:

Facial landmark detection algorithms seek to identify the locations of the facial key landmark points on facial images or videos. Such key points are the main points that describe the unique location of a facial component (e.g., eye corner), or an interpolated point that connects those dominant points around the facial components, as well as facial contour. Formally, given a facial image denoted as I, a landmark detection algorithm predicts the locations of D landmarks x={x1,y1,x2,y2,⋯,xD,yD}, where x and y represent the image coordinates of the facial landmarks. 

![](https://miro.medium.com/max/3840/1*stAhFcyYdNqGvV26xMdE7w.png)

In the above picture, one can clearly see that how an algorihtm is identifying the range of every feature.

The concept of identifying these points is similar to the concept of face detection, but in face detection, only these features are just identified, 
but in face-landmark we also find the distace or the boundary of each feature.
So, we can say that face landmark detection is the process of finding points of interest in an image of a human face.
This can be useful to detect emotion through facial gestures, estimating gaze direction, changing facial appearance (face swap), augmenting faces with graphics, and puppeteering of virtual characters.