# Face - Recognition:

Face recognition is a method of identifying or verifying the identity of an individual using their face.

## Basic working: 

![](https://kevinpatel.me/img/blog-details/face-recognition-using-deep-learning/1.webp)

These are the following steps for face recognition - 

**Step 1.** A picture of your face is captured from a photo or video. Your face might appear alone or in a crowd. Your image may show you looking straight ahead or nearly in profile.

**Step 2.** Facial recognition software reads the geometry of your face. Key factors include the distance between your eyes and the distance from forehead to chin. The software identifies facial landmarks — one system identifies 68 of them — that are key to distinguishing your face. The result: your facial signature.

**Step 3.** Your facial signature — a mathematical formula — is compared to a database of known faces. And consider this: at least 117 million Americans have images of their faces in one or more police databases. According to a May 2018 report, the FBI has had access to 412 million facial images for searches.

**Step 4.** A determination is made. Your faceprint may match that of an image in a facial recognition system database.

## How a computer understands the features: 

![](https://www.fingertec.com/companyprofile/development/images/bio-03-01.jpg)

As depicted in the above picture, each pixel is to be considered while deciding the features of a person.
For eg: 
if a model have decided the location of the eye in a particular picture, then it will mark as 1 to that pixel. and other surrounding will be 
unmarked. So, a system gets an input in the form of 1's and 0's.

After this, when these arrangement of 1's and 0's for is  saved, then when a new images of that person is given, then these pixels are compared
with the new image.