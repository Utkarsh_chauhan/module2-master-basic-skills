# Image - Classify:

Images can be easily classified using the concept of CNN.

For eg: we want to classify an image whether it is dog or a cat.
So, we can create a CNN, which identifies and then classify whether the given image is dog/cat.

In this each pixel is checked and weighted in hidden layer of CNN, and adjusted till we get a considerable of good accuracy.
Before the output layer we can add binary acitvation function so as to get the answer in the form of 1 and 0, where 0 will denote a dog and 1 will
denote a cat.

![](https://www.mdpi.com/sensors/sensors-19-04933/article_deploy/html/images/sensors-19-04933-g001.png)

In the above picture one can clearly, see the different layers of CNN, and how each pixel is being weighted, and as an output we are able to classify whether the place is damaged, healthy, or in danger etc.

So with the help of CNN we can easily classify images accordingly.

